# Utilisation d'une image de base avec Node.js
FROM node:14-alpine

# Création du répertoire de travail dans le conteneur
WORKDIR /app

# Copie des fichiers package.json et package-lock.json dans le répertoire de travail
COPY package*.json ./

# Installation des dépendances ou la miser a jours des dépendances 
#  docker run -d -p 3000:3000 mon_application_react
RUN npm install
RUN npm update

# Copie de tous les fichiers de l'application dans le répertoire de travail
COPY . .

# Construction de l'application React
RUN npm run build

# Commande pour exécuter l'application lorsqu'elle est lancée dans le conteneur
CMD ["npm", "start"]
